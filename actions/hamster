#!/bin/bash

shift
action=$1
shift
item=$1
shift
param="$*"

function usage {
    echo ""
    echo "  $(basename $0) <HAMSTEAR ACTION> <ITEM> <HAMSTER OPTIONS>"
    echo "    Start the selected task in Hamster Time Tracker"
    echo "    (http://projecthamster.wordpress.com/)."
    echo "    Relies on Hamster being installed and excecutable"
    echo "    from the command line with the command: hamster"
    echo "    Uses the project of the task as the Hamster category."
    echo "    <HAMSTER OPTIONS> are hamster start options, such as time started."
    echo "    Details for these options can be found in hamster --help"
    echo ""
    echo "    Examples:"
    echo "      $ $TODO_SH $(basename $0) start 42"
    echo "    Starts task 42 in hamster"
    echo "      $ $TODO_SH $(basename $0) start 42 15:25"
    echo "    Starts task 42 in hamster with a start time of 15:25"
    echo "      $ $TODO_SH $(basename $0) stop"
    echo "    Stop current task"
    echo ""
    exit
}

if [[ "$action" = start ]]; then
    if ! [[ "$item" =~ ^[0-9]+$ ]]; then
        echo "Error! Usage:"
        usage
        exit
    fi

    getTodo $item

    # cut off the priority
    if [[ "$todo" =~ ^\([A-Z]\) ]]; then
        todo=$(echo "$todo" | cut -f2- -d ' ')
    fi

    # cut off the contexts
    tags=""
    if [[ "$todo" =~ @ ]]; then
        tags=$(echo "$todo" | grep -o '@\w\+' | sed 's/@/#/g' | xargs)
        todo=$(echo "$todo" | sed -r 's/@\w*?\s?//g')
    fi

    # cut off the projects
    project=""
    if [[ "$todo" =~ \+ ]]; then
        project=$(echo "$todo" | grep -o '+[^ ]\+' | head -1 | sed 's/+//g' | sed 's/-/@/g')
        todo=$(echo "$todo" | sed -r 's/\+[^ ]*?\s?//g')
    fi

    # cut off time strings starting with m:YYYY.MM.DD
    if [[ "$todo" =~ : ]]; then
        todo=$(echo "$todo" | sed 's/\w:\([0-9]\{2,4\}[^A-Za-z0-9]\)\{2\}[0-9]\{2,4\}\s\?//g')
    fi

    param="'$project, $todo $tags' $param"
fi

if type -P hamster-cli>/dev/null; then
    hamster-cli $action $param
fi
